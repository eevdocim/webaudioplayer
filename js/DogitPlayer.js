let tracks = []; //массив с плейлистом
let tracks_name = []; //массив с именами треков
let t = 0; //номер играющего трэка
let song = new Audio();//аудио плеер
let repeat = false; //повторение альбома
let rewind = false; //перемотка трека
let Loaded = false;//загрузка трека
let currentTimeAudio = 0; //текущая время трека для перемотки колёсиком мыши
let the_title = null //заголовок страницы
let domain = null //домен плеера
let test = false; //для тестов
//макет плеера
let player = null
let name_track = null
let time = null
let current_time = null
let duration_time = null
let difference_time = null
let btPrev = null
let btPlay = null
let btNext = null
let btDuration = null
let btVolumeControl = null
let btVolume = null

function DogitPlayer(){
    player = document.createElement('div');
    player.id = "audioplayer";
    the_title = document.querySelector("title");
    song.volume = 0.2; //громкость плеера по умолчанию

    //заполнение макета
    player.innerHTML = "\
    <link href="+ (domain == null ? './css/DogitPlayer.css' : domain + '/css/DogitPlayer.css')+"  rel='stylesheet'/>\
    <button disabled=true id='btPrev' class='Btdisabled transition' onClick='Prev()'>\
        <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512'>\
            <path fill='currentColor' d='M11.5 280.6l192 160c20.6 17.2 52.5 2.8 52.5-24.6V96c0-27.4-31.9-41.8-52.5-24.6l-192 160c-15.3 12.8-15.3 36.4 0 49.2zm256 0l192 160c20.6 17.2 52.5 2.8 52.5-24.6V96c0-27.4-31.9-41.8-52.5-24.6l-192 160c-15.3 12.8-15.3 36.4 0 49.2z' />\
        </svg>\
    </button>\
    <button disabled=true id='btPlay' class='Btdisabled transition' onClick='Play()'>\
        <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 448 512'>\
            <path fill='currentColor' d='M424.4 214.7L72.4 6.6C43.8-10.3 0 6.1 0 47.9V464c0 37.5 40.7 60.1 72.4 41.3l352-208c31.4-18.5 31.5-64.1 0-82.6z' />\
        </svg>\
    </button>\
    <button disabled=true id='btNext' class='Btdisabled transition' onClick='Next()'>\
        <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512'>\
            <path fill='currentColor' d='M500.5 231.4l-192-160C287.9 54.3 256 68.6 256 96v320c0 27.4 31.9 41.8 52.5 24.6l192-160c15.3-12.8 15.3-36.4 0-49.2zm-256 0l-192-160C31.9 54.3 0 68.6 0 96v320c0 27.4 31.9 41.8 52.5 24.6l192-160c15.3-12.8 15.3-36.4 0-49.2z' />\
        </svg>\
    </button>\
    <div class='player'>\
        <span id='name_track'>Плеер</span>\
        <input " + (test == false ? 'disabled=true':'') +" id='btDuration' type='range' min='0' max='0' step='0.1' value='0' onChange='ChangeTime()'>\
        <span id='test_time'>\
            <span id='t_seconds'>0</span>\
            <span>||</span>\
            <span id='t_current_time'>0</span>\
            <span>||</span>\
            <span id='t_difference_time'>0</span>\
            <span>||</span>\
            <span id='t_duration_time'>0</span>\
        </span>\
        <span id='time' style='opacity: 0;'>\
            <span class='hide' id='current_time'>00:00</span>\
            <span class='hide' id='duration_time'>00:00</span>\
            <span class='show' id='difference_time'>00:00</span>\
        </span>\
    </div>\
    <button id='btVolumeControl'>\
        <svg width='18' height='18' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 576 512'>\
            <path fill='currentColor' d='M215.03 71.05L126.06 160H24c-13.26 0-24 10.74-24 24v144c0 13.25 10.74 24 24 24h102.06l88.97 88.95c15.03 15.03 40.97 4.47 40.97-16.97V88.02c0-21.46-25.96-31.98-40.97-16.97zm233.32-51.08c-11.17-7.33-26.18-4.24-33.51 6.95-7.34 11.17-4.22 26.18 6.95 33.51 66.27 43.49 105.82 116.6 105.82 195.58 0 78.98-39.55 152.09-105.82 195.58-11.17 7.32-14.29 22.34-6.95 33.5 7.04 10.71 21.93 14.56 33.51 6.95C528.27 439.58 576 351.33 576 256S528.27 72.43 448.35 19.97zM480 256c0-63.53-32.06-121.94-85.77-156.24-11.19-7.14-26.03-3.82-33.12 7.46s-3.78 26.21 7.41 33.36C408.27 165.97 432 209.11 432 256s-23.73 90.03-63.48 115.42c-11.19 7.14-14.5 22.07-7.41 33.36 6.51 10.36 21.12 15.14 33.12 7.46C447.94 377.94 480 319.54 480 256zm-141.77-76.87c-11.58-6.33-26.19-2.16-32.61 9.45-6.39 11.61-2.16 26.2 9.45 32.61C327.98 228.28 336 241.63 336 256c0 14.38-8.02 27.72-20.92 34.81-11.61 6.41-15.84 21-9.45 32.61 6.43 11.66 21.05 15.8 32.61 9.45 28.23-15.55 45.77-45 45.77-76.88s-17.54-61.32-45.78-76.86z'/>\
        </svg>\
    </button>\
    <input id='btVolume' type='range' min='0' max='1' step='0.001' value=" + song.volume + " onChange='ChangeVolume()'>\
    <button id='btRepeat' onclick='Repeat()'>\
        <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512'>\
            <path fill='currentColor' d='M197.116,7.461c60.933,0,118.212,28.518,155.232,75.784l56.682-32.754l0.121,187.495l-162.438-93.645l52.206-30.21 c-25.646-28.724-62.548-46.02-101.803-46.02c-75.25,0-136.465,61.213-136.465,136.465c0,75.25,61.216,136.468,136.465,136.468 v60.645C88.432,401.689,0,313.267,0,204.577C0,95.891,88.429,7.461,197.116,7.461z'/>\
        </svg >\
    </button>\
    ";
    document.body.append(player);
    audioPlayer = document.getElementById('audioplayer');
    name_track = document.getElementById('name_track');
    time = document.getElementById('time');
    current_time = document.getElementById('current_time');
    duration_time = document.getElementById('duration_time');
    difference_time = document.getElementById('difference_time');
    btPrev = document.getElementById('btPrev');
    btPlay = document.getElementById('btPlay');
    btNext = document.getElementById('btNext');
    btDuration = document.getElementById('btDuration');
    btVolumeControl = document.getElementById('btVolumeControl');
    btVolume = document.getElementById('btVolume');
    if (test) {
        test_time = document.getElementById('test_time');
        btDuration.setAttribute('max', 1000);
        btPlay.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><circle cx="12" cy="2" r="0" fill="currentColor"><animate attributeName="r" begin="0" calcMode="spline" dur="1s" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" repeatCount="indefinite" values="0;2;0;0"/></circle><circle cx="12" cy="2" r="0" fill="currentColor" transform="rotate(45 12 12)"><animate attributeName="r" begin="0.125s" calcMode="spline" dur="1s" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" repeatCount="indefinite" values="0;2;0;0"/></circle><circle cx="12" cy="2" r="0" fill="currentColor" transform="rotate(90 12 12)"><animate attributeName="r" begin="0.25s" calcMode="spline" dur="1s" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" repeatCount="indefinite" values="0;2;0;0"/></circle><circle cx="12" cy="2" r="0" fill="currentColor" transform="rotate(135 12 12)"><animate attributeName="r" begin="0.375s" calcMode="spline" dur="1s" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" repeatCount="indefinite" values="0;2;0;0"/></circle><circle cx="12" cy="2" r="0" fill="currentColor" transform="rotate(180 12 12)"><animate attributeName="r" begin="0.5s" calcMode="spline" dur="1s" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" repeatCount="indefinite" values="0;2;0;0"/></circle><circle cx="12" cy="2" r="0" fill="currentColor" transform="rotate(225 12 12)"><animate attributeName="r" begin="0.625s" calcMode="spline" dur="1s" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" repeatCount="indefinite" values="0;2;0;0"/></circle><circle cx="12" cy="2" r="0" fill="currentColor" transform="rotate(270 12 12)"><animate attributeName="r" begin="0.75s" calcMode="spline" dur="1s" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" repeatCount="indefinite" values="0;2;0;0"/></circle><circle cx="12" cy="2" r="0" fill="currentColor" transform="rotate(315 12 12)"><animate attributeName="r" begin="0.875s" calcMode="spline" dur="1s" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" repeatCount="indefinite" values="0;2;0;0"/></circle></svg>'
        t_seconds = document.getElementById('t_seconds');
        t_current_time = document.getElementById('t_current_time');
        t_difference_time = document.getElementById('t_difference_time');
        t_duration_time = document.getElementById('t_duration_time');
        time.style.opacity = 100;
    }
    if(!test){
        setInterval(function () {
            if (!song.paused){
                
                if(rewind){
                    current_time.innerText = Time(btDuration.value);
                    difference_time.innerText = Time(btDuration.value);
                }else{
                    current_time.innerText = Time(song.currentTime);
                    difference_time.innerText = DifferenceTime(song.currentTime);
                }
                btDuration.setAttribute('max', song.duration);
            }
            if (!rewind) {
                btDuration.value = song.currentTime;
                currentTimeAudio = song.currentTime;
            }
            showRangeProgress(btDuration);
            if (song.ended && Loaded) {
                if (tracks.length > 0) {
                    if (t < tracks.length - 1) {
                        t++;
                        player.setAttribute('src', tracks[t]);
                        Load()
                    } else {
                        t = 0;
                        if (repeat) {
                            player.setAttribute('src', tracks[t]);
                            Load()
                        } else {
                            //блокирует плеер
                            Stop()
                        }
                    }
                }
            }
        }, 100);
    }

    //Перемещение ползунков
    const showRangeProgress = (rangeInput) => {
        if (rangeInput === btDuration){
            audioPlayer.style.setProperty('--duration-before-width', rangeInput.value / rangeInput.max * 100 + '%');
            if(test){
                t_seconds.innerText = rangeInput.value;
                current_time.innerText = t_current_time.innerText = Time(rangeInput.value);
                difference_time.innerText = t_difference_time.innerText = DifferenceTime(rangeInput.value, rangeInput.max);
                duration_time.innerText = t_duration_time.innerText = Time(rangeInput.max);
            }
        }
        else{
            audioPlayer.style.setProperty('--volume-before-width', rangeInput.value / rangeInput.max * 100 + '%');
        }
        
    }

    btDuration.addEventListener('input', (e) => {
        showRangeProgress(e.target);
    });
    btVolume.addEventListener('input', (e) => {
        showRangeProgress(e.target);
    });

    //Управление плеером
    document.addEventListener('keydown', function (event) {
        if (event.code == 'Space') {
            if (document.activeElement.id == 'btPlay' || btPlay.getAttribute('disabled')) return true;
            Play();
        }
        if (event.code == 'KeyB') {
            Prev();
        }
        if (event.code == 'KeyN') {
            Next();
        }
    });

    btVolumeControl.addEventListener('mouseup', function (event) {
        e = event || window.event;
        if (song.volume == 0) {
            btVolume.removeAttribute('disabled')
            song.volume = btVolume.value
            btVolumeControl.innerHTML = "<svg width='20' height='20' viewBox='0 0 18 15' fill='none' xmlns='http://www.w3.org/2000/svg'>\
        <path fill-rule='evenodd' clip-rule='evenodd' d='M8.383 0.439786C8.56569 0.515531 8.72181 0.643743 8.83163 0.808212C8.94146 0.97268 9.00005 1.16602 9 1.36379V13.3638C8.99996 13.5615 8.94129 13.7548 8.8314 13.9192C8.72152 14.0837 8.56535 14.2118 8.38265 14.2875C8.19995 14.3631 7.99892 14.3829 7.80497 14.3444C7.61102 14.3058 7.43285 14.2106 7.293 14.0708L3.586 10.3638H1C0.734784 10.3638 0.48043 10.2584 0.292893 10.0709C0.105357 9.88336 0 9.629 0 9.36379V5.36379C0 5.09857 0.105357 4.84422 0.292893 4.65668C0.48043 4.46914 0.734784 4.36379 1 4.36379H3.586L7.293 0.656786C7.43285 0.516854 7.61105 0.421545 7.80508 0.382918C7.9991 0.344291 8.20023 0.364082 8.383 0.439786V0.439786ZM13.657 0.292786C13.8445 0.105315 14.0988 0 14.364 0C14.6292 0 14.8835 0.105315 15.071 0.292786C16.0008 1.22044 16.7382 2.32264 17.2409 3.5361C17.7435 4.74955 18.0015 6.05035 18 7.36379C18.0015 8.67723 17.7435 9.97802 17.2409 11.1915C16.7382 12.4049 16.0008 13.5071 15.071 14.4348C14.8824 14.6169 14.6298 14.7177 14.3676 14.7155C14.1054 14.7132 13.8546 14.608 13.6692 14.4226C13.4838 14.2372 13.3786 13.9864 13.3763 13.7242C13.374 13.462 13.4748 13.2094 13.657 13.0208C14.4011 12.2788 14.9912 11.3971 15.3933 10.4262C15.7955 9.45539 16.0016 8.41461 16 7.36379C16.0017 6.31295 15.7955 5.27216 15.3934 4.30131C14.9913 3.33045 14.4012 2.4487 13.657 1.70679C13.4695 1.51926 13.3642 1.26495 13.3642 0.999786C13.3642 0.734622 13.4695 0.480314 13.657 0.292786V0.292786ZM10.828 3.12079C10.9209 3.02781 11.0312 2.95405 11.1526 2.90373C11.274 2.8534 11.4041 2.8275 11.5355 2.8275C11.6669 2.8275 11.797 2.8534 11.9184 2.90373C12.0398 2.95405 12.1501 3.02781 12.243 3.12079C12.8009 3.67745 13.2433 4.33885 13.5448 5.067C13.8463 5.79514 14.001 6.57568 14 7.36379C14.001 8.15188 13.8463 8.93241 13.5447 9.66055C13.2432 10.3887 12.8008 11.0501 12.243 11.6068C12.0554 11.7944 11.8009 11.8998 11.5355 11.8998C11.2701 11.8998 11.0156 11.7944 10.828 11.6068C10.6404 11.4191 10.5349 11.1647 10.5349 10.8993C10.5349 10.6339 10.6404 10.3794 10.828 10.1918C11.2002 9.82096 11.4953 9.38019 11.6965 8.89485C11.8976 8.4095 12.0008 7.88916 12 7.36379C12.0008 6.8384 11.8977 6.31805 11.6965 5.8327C11.4954 5.34734 11.2002 4.90658 10.828 4.53579C10.735 4.44291 10.6613 4.33262 10.6109 4.21123C10.5606 4.08983 10.5347 3.9597 10.5347 3.82829C10.5347 3.69687 10.5606 3.56674 10.6109 3.44535C10.6613 3.32395 10.735 3.21366 10.828 3.12079V3.12079Z' fill='black'/>\
        </svg>"
        } else {
            song.volume = 0
            btVolume.setAttribute('disabled', true)
            btVolumeControl.innerHTML = "<svg width='20' height='20' viewBox='0 0 20 20' fill='none' xmlns='http://www.w3.org/2000/svg'>\
        <path fill-rule='evenodd' clip-rule='evenodd' d='M9.383 3.076C9.56569 3.15175 9.72181 3.27996 9.83163 3.44443C9.94146 3.6089 10 3.80224 10 4V16C9.99996 16.1978 9.94129 16.3911 9.8314 16.5555C9.72152 16.7199 9.56535 16.848 9.38265 16.9237C9.19995 16.9993 8.99892 17.0192 8.80497 16.9806C8.61102 16.942 8.43285 16.8468 8.293 16.707L4.586 13H2C1.73478 13 1.48043 12.8946 1.29289 12.7071C1.10536 12.5196 1 12.2652 1 12V8C1 7.73478 1.10536 7.48043 1.29289 7.29289C1.48043 7.10536 1.73478 7 2 7H4.586L8.293 3.293C8.43285 3.15307 8.61105 3.05776 8.80508 3.01913C8.9991 2.98051 9.20023 3.0003 9.383 3.076V3.076ZM12.293 7.293C12.4805 7.10553 12.7348 7.00022 13 7.00022C13.2652 7.00022 13.5195 7.10553 13.707 7.293L15 8.586L16.293 7.293C16.3852 7.19749 16.4956 7.12131 16.6176 7.0689C16.7396 7.01649 16.8708 6.9889 17.0036 6.98775C17.1364 6.9866 17.2681 7.0119 17.391 7.06218C17.5138 7.11246 17.6255 7.18671 17.7194 7.28061C17.8133 7.3745 17.8875 7.48615 17.9378 7.60905C17.9881 7.73194 18.0134 7.86362 18.0123 7.9964C18.0111 8.12918 17.9835 8.2604 17.9311 8.38241C17.8787 8.50441 17.8025 8.61475 17.707 8.707L16.414 10L17.707 11.293C17.8892 11.4816 17.99 11.7342 17.9877 11.9964C17.9854 12.2586 17.8802 12.5094 17.6948 12.6948C17.5094 12.8802 17.2586 12.9854 16.9964 12.9877C16.7342 12.99 16.4816 12.8892 16.293 12.707L15 11.414L13.707 12.707C13.5184 12.8892 13.2658 12.99 13.0036 12.9877C12.7414 12.9854 12.4906 12.8802 12.3052 12.6948C12.1198 12.5094 12.0146 12.2586 12.0123 11.9964C12.01 11.7342 12.1108 11.4816 12.293 11.293L13.586 10L12.293 8.707C12.1055 8.51947 12.0002 8.26517 12.0002 8C12.0002 7.73484 12.1055 7.48053 12.293 7.293V7.293Z' fill='black'/>\
        </svg>"
        }
        e.preventDefault ? e.preventDefault() : (e.returnValue = false);
    });

    btVolume.addEventListener('wheel', function (event) {
        if (btVolume.getAttribute('disabled')) return true;
        e = event || window.event;
        let delta = e.deltaY || e.detail || e.wheelDelta;
        delta = delta / 10000 * -1;
        if ((song.volume >= 0 && delta < 0) || (song.volume <= 1 && delta > 0)) {
            speed = delta * 5;
            if (song.volume + speed < 0) {
                song.volume = 0;
            } else if (song.volume + speed > 1) {
                song.volume = 1;
            }else{
                song.volume += speed;
            }
            btVolume.value = song.volume;
        }
        showRangeProgress(btVolume);
        e.preventDefault ? e.preventDefault() : (e.returnValue = false);
    });

    btDuration.addEventListener('wheel', function (event) {
        e = event || window.event;
        rewind = true;
        let delta = e.deltaY || e.detail || e.wheelDelta;
        delta = delta / 100 * -1;
        if ((btDuration.value > 0 && delta < 0) || (btDuration.value < song.duration && delta > 0)) {
            currentTimeAudio += delta;
            btDuration.value = currentTimeAudio
        }
        e.preventDefault ? e.preventDefault() : (e.returnValue = false);
    });

    btDuration.addEventListener('mousedown', function (event) {
        if(test) return;
        rewind = true
        e = event || window.event;
        btDuration.addEventListener('mousemove', btDuration.fn = function (event) {
            e = event || window.event;
            if (song.src) {
                let delta = (e.pageX - $(this).offset().left) / ($(this).outerWidth() / song.duration);
                btDuration.value = delta
            }
            e.preventDefault ? e.preventDefault() : (e.returnValue = false);
        });
        e.preventDefault ? e.preventDefault() : (e.returnValue = false);
    });

    btDuration.addEventListener('mouseup', function (event) {
        if (test) return;
        e = event || window.event;
        btDuration.removeEventListener('mousemove', btDuration.fn, false);
        if (song.src) {
            let delta = (e.pageX - $(this).offset().left) / ($(this).outerWidth() / song.duration);
            btDuration.value = delta
        }
        if (rewind)
            ChangeTime()
        rewind = false
        e.preventDefault ? e.preventDefault() : (e.returnValue = false);
    });

    btDuration.addEventListener('mouseover', function (event) {
        e = event || window.event;
        difference_time.classList.remove("show");
        difference_time.classList.add("hide");
        current_time.classList.remove("hide");
        current_time.classList.add("show");
        duration_time.classList.remove("hide");
        duration_time.classList.add("show");
        e.preventDefault ? e.preventDefault() : (e.returnValue = false);
    });

    btDuration.addEventListener('mouseout', function (event) {
        e = event || window.event;
        difference_time.classList.remove("hide");
        difference_time.classList.add("show");
        current_time.classList.remove("show");
        current_time.classList.add("hide");
        duration_time.classList.remove("show");
        duration_time.classList.add("hide");
        btDuration.removeEventListener('mousemove', btDuration.fn, false);
        if (rewind)
            ChangeTime()
        rewind = false
        e.preventDefault ? e.preventDefault() : (e.returnValue = false);
    });
}

function Repeat() {
    repeat = !repeat
    if (repeat) {
        document.getElementById('btRepeat').classList.add('press');
    } else {
        document.getElementById('btRepeat').classList.remove('press');
    }
}

function ChangeTime() {
    song.currentTime = btDuration.value;
}

function ChangeVolume() {
    song.volume = btVolume.value;
}

function Load() {
    Loaded = false;
    Stop()
    btPlay.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><circle cx="12" cy="2" r="0" fill="currentColor"><animate attributeName="r" begin="0" calcMode="spline" dur="1s" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" repeatCount="indefinite" values="0;2;0;0"/></circle><circle cx="12" cy="2" r="0" fill="currentColor" transform="rotate(45 12 12)"><animate attributeName="r" begin="0.125s" calcMode="spline" dur="1s" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" repeatCount="indefinite" values="0;2;0;0"/></circle><circle cx="12" cy="2" r="0" fill="currentColor" transform="rotate(90 12 12)"><animate attributeName="r" begin="0.25s" calcMode="spline" dur="1s" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" repeatCount="indefinite" values="0;2;0;0"/></circle><circle cx="12" cy="2" r="0" fill="currentColor" transform="rotate(135 12 12)"><animate attributeName="r" begin="0.375s" calcMode="spline" dur="1s" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" repeatCount="indefinite" values="0;2;0;0"/></circle><circle cx="12" cy="2" r="0" fill="currentColor" transform="rotate(180 12 12)"><animate attributeName="r" begin="0.5s" calcMode="spline" dur="1s" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" repeatCount="indefinite" values="0;2;0;0"/></circle><circle cx="12" cy="2" r="0" fill="currentColor" transform="rotate(225 12 12)"><animate attributeName="r" begin="0.625s" calcMode="spline" dur="1s" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" repeatCount="indefinite" values="0;2;0;0"/></circle><circle cx="12" cy="2" r="0" fill="currentColor" transform="rotate(270 12 12)"><animate attributeName="r" begin="0.75s" calcMode="spline" dur="1s" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" repeatCount="indefinite" values="0;2;0;0"/></circle><circle cx="12" cy="2" r="0" fill="currentColor" transform="rotate(315 12 12)"><animate attributeName="r" begin="0.875s" calcMode="spline" dur="1s" keySplines="0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8;0.2 0.2 0.4 0.8" repeatCount="indefinite" values="0;2;0;0"/></circle></svg>'
    fetch(player.getAttribute('src'))
        .then(response => response.blob())
        .then(blob => {
            song.src = URL.createObjectURL(blob);
            return song.play();
        })
        .then(_ => {
            the_title.innerHTML = tracks_name[t].artist + '-' + tracks_name[t].name;
            name_track.innerText = t + 1 + '.' + tracks_name[t].name;
            btPlay.innerHTML = "<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 448 512'>\
            <path fill='currentColor' d='M144 479H48c-26.5 0-48-21.5-48-48V79c0-26.5 21.5-48 48-48h96c26.5 0 48 21.5 48 48v352c0 26.5-21.5 48-48 48zm304-48V79c0-26.5-21.5-48-48-48h-96c-26.5 0-48 21.5-48 48v352c0 26.5 21.5 48 48 48h96c26.5 0 48-21.5 48-48z'\>\
            </svg>"
            btPlay.classList.remove("Btdisabled");
            btPlay.removeAttribute('disabled');
            btPrev.classList.remove("Btdisabled");
            btPrev.removeAttribute('disabled');
            btDuration.removeAttribute('disabled');
            if (tracks.length > 1) {
                btNext.classList.remove("Btdisabled");
                btNext.removeAttribute('disabled');
            } else {
                btNext.classList.add("Btdisabled");
                btNext.setAttribute('disabled', true);
            }
            duration_time.innerText = Time(song.duration);
            time.style.opacity = 100;
            Loaded = true;
        })
        .catch(e => {
            alert("Ошибка загрузки трека");
            console.log(e);
        })
}

function DifferenceTime(time, duration = null){
    if(duration == null){
        duration = song.duration;
    }
    time = duration - time;
    return Time(time)
}

function Time(time) {
    minute = (time / 60) | 0;
    if(minute > 0){
        seconds = (time % 60) | 0;
    }else{
        seconds = (time % 60).toFixed(0);
    }
    sec = addZero(seconds);
    return minute + ":" + sec;
}

function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function Play() {
    if (!song.paused) {
        btPlay.innerHTML = "<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 448 512'>\
        <path fill='currentColor' d='M424.4 214.7L72.4 6.6C43.8-10.3 0 6.1 0 47.9V464c0 37.5 40.7 60.1 72.4 41.3l352-208c31.4-18.5 31.5-64.1 0-82.6z'/>\
        </svg>"
        song.pause();
    } else {
        btPlay.innerHTML = "<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 448 512'>\
        <path fill='currentColor' d='M144 479H48c-26.5 0-48-21.5-48-48V79c0-26.5 21.5-48 48-48h96c26.5 0 48 21.5 48 48v352c0 26.5-21.5 48-48 48zm304-48V79c0-26.5-21.5-48-48-48h-96c-26.5 0-48 21.5-48 48v352c0 26.5 21.5 48 48 48h96c26.5 0 48-21.5 48-48z'/>\
        </svg>"
        song.play()
    }
}

function Next() {
    if (tracks && tracks.length > 1) {
        if (t < tracks.length - 1) {
            t++;
            player.setAttribute('src', tracks[t]);
            Load()
        } else {
            t = 0;
            player.setAttribute('src', tracks[t]);
            Load()
        }
    }
}

function Prev() {
    if (tracks) {
        if (t > 0) {
            t--;
            player.setAttribute('src', tracks[t]);
            Load()
        } else {
            t = tracks.length - 1;
            player.setAttribute('src', tracks[t]);
            Load()
        }
    }
}

function Stop(){
    if (!song.paused) {
        song.pause()
    }
    song.src = "";
    btPlay.innerHTML = "<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 448 512'>\
                            <path fill='currentColor' d='M424.4 214.7L72.4 6.6C43.8-10.3 0 6.1 0 47.9V464c0 37.5 40.7 60.1 72.4 41.3l352-208c31.4-18.5 31.5-64.1 0-82.6z'\>\
                            </svg>"
    btPrev.setAttribute('disabled', true);
    btPrev.classList.add("Btdisabled");
    btPlay.setAttribute('disabled', true);
    btPlay.classList.add("Btdisabled");
    btNext.setAttribute('disabled', true);
    btNext.classList.add("Btdisabled");
    btDuration.setAttribute('disabled', true);
    name_track.innerText = 'Плеер';
    time.style.opacity=0;
}

function updateTheme() {
    const colorMode = window.matchMedia("(prefers-color-scheme: dark)").matches ?
        "dark" :
        "light";
    document.querySelector("html").setAttribute("data-bs-theme", colorMode);
}